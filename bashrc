#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias l='ls --color=auto'
alias la='ls -a --color=auto'
alias mk='mkdir'
alias rmr='rm -r'
alias e='exit'
alias vim='nvim'
alias vi='nvim'
alias vs='if [ -e "Session.vim" ]; then vim -S; else vim; fi'
alias x='xfce4-panel & exit'
alias ta='tmux a'
alias tn='tmux new -s main'
alias jd='mv */* ./'
alias ddinfo='sudo kill -USR1 $(pidof dd)'
alias dd='dd status=progress'
alias sudo='sudo -E'
alias su='su -m'
alias y='yaourt'
alias u='yaourt -Syua --noconfirm'
alias ue='yaourt -Syua --noconfirm && exit'
alias mp3='mp3gain -a *.mp3'
alias flac='find $1 -iname "*.flac" -execdir metaflac --add-replay-gain {} \+'
alias flactomp3='whatmp3 -n --V0 .'
alias hw='inxi -Fx'
alias fixsteam='find ~/.steam/root/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" -o -name "libgpg-error.so*" \) -print -delete'
alias vpn='~/dotfiles/vpn.sh'
alias vimgolf='~/.gem/ruby/2.3.0/bin/vimgolf'
alias addtobeginning=addtobeginningfunction
alias cmus-update='rm -rf ~/.config/cmus && read -p "Start cmus and press [Enter]" && cmus-remote -l "/home/robert/Music"'
alias converttox264=converttox264function
alias convertalltox264=convertalltox264function
alias generatepw='pwgen -s -y 12'
alias hosts='cd /home/robert/dev/hosts && cp whitelist .. && cp myhosts .. && git reset --hard HEAD && git pull && mv ../whitelist . && mv ../myhosts . && python3 updateHostsFile.py -a -r -b -e gambling && cd -'
alias uh='yaourt -Syua --noconfirm && cd /home/robert/dev/hosts && cp whitelist .. && cp myhosts .. && git reset --hard HEAD && git pull && mv ../whitelist . && mv ../myhosts . && python3 updateHostsFile.py -a -r -b -e gambling && cd -'
alias uhe='yaourt -Syua --noconfirm && cd /home/robert/dev/hosts && python3 updateHostsFile.py -a -r -b -e gambling && exit'
alias blankhost='sudo mv /etc/hosts /etc/hosts.tmp && sudo cp /etc/hosts.blank /etc/hosts && read -p "Press [Enter] to revert to normal hosts file" && sudo mv /etc/hosts.tmp /etc/hosts'
alias doc='jabref > /dev/null 2>&1 & cd ~/Doktorarbeit/Arbeit/999-Arbeit/ && tmux'
alias spell='aspell --lang=de_DE --mode=tex check'
alias party='cd ~/Dropbox/Meine_Daten/backup/xbmc/makePlaylist && ./makePlaylist.sh && scp PartyMode.xsp root@kodi:~/.kodi/userdata && cd -'
alias pi='ssh robert@pi'
alias ht='ssh robert@htpc'
alias mm='ssh mm@htpc'
alias wake='wol D0:50:99:93:B0:9C'
alias wht='wol D0:50:99:93:B0:9C; while [ 1 ]; do ping -q -c 1 -W 1 htpc > /dev/null; if [ $? -eq 0 ]; then break; fi; sleep 1; done; ssh robert@htpc'
alias wmm='wol D0:50:99:93:B0:9C; while [ 1 ]; do ping -q -c 1 -W 1 htpc > /dev/null; if [ $? -eq 0 ]; then break; fi; sleep 1; done; ssh mm@htpc'
alias whz='wol D0:50:99:93:B0:9C; while [ 1 ]; do ping -q -c 1 -W 1 htpc > /dev/null; if [ $? -eq 0 ]; then break; fi; sleep 1; done; filezilla & exit'
alias removeunused='sudo pacman -Rns $(pacman -Qtdq)'
alias cpr='rsync -Pr'
alias backup='cd /run/media/mm/ && rsync -av --delete backup/usb/ 269aab9f-5e9b-4335-bf88-4bf21c187198/ && cd -'
alias usb='cd /run/media/mm/backup/usb'
alias receive='nc -l -p 7000 | pv | tar x'
alias send='tar cf - * | pv | nc htpc 7000'
alias man='man -P most'
alias bib='rsync -a -e ssh ~/Doktorarbeit/Arbeit/999-Arbeit/bibtex.bib htpc:~/Doktorarbeit/Arbeit/999-Arbeit/bibtex.bib'
alias filebot='filebot -rename -non-strict'
alias tagger='/home/robert/dev/tagger/tagger.py'
alias namer='/home/robert/dev/namer/namer.py'
alias kodi-update-audio='curl -s --user kodi:Ni3P5%Wp --header "Content-Type: application/json" --data-binary "{\"id\":1, \"jsonrpc\":\"2.0\", \"method\": \"AudioLibrary.Scan\", \"params\": {}}" "http://htpc:8080/jsonrpc" > /dev/null'
alias kodi-update-video='curl -s --user kodi:Ni3P5%Wp --header "Content-Type: application/json" --data-binary "{\"id\":1, \"jsonrpc\":\"2.0\", \"method\": \"VideoLibrary.Scan\", \"params\": {}}" "http://htpc:8080/jsonrpc" > /dev/null'
alias gspdf='gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -CompatibilityLebel=1.4 -dPDFSETTINGS=/printer -sPAPERSIZE=a4 -sOutputFile=out.pdf'

addtobeginningfunction() {
	for f in *.*; do mv "$f" "$1 - $f"; done
}

converttox264function() {
	ffmpeg -i "$1" -acodec copy -vcodec libx264 "$1-x264.mkv"
}

convertalltox264function() {
	for f in *.*; do ffmpeg -i "$f" -acodec copy -vcodec libx264 "$f-x264.mkv"; done
}

. ~/.git-completion.bash
. ~/.git-prompt.sh
export GIT_PS1_SHOWDIRTYSTATE=1

PS1='\[\033[34m\][\u@\h \W]\$\[\033[0m\] '
PROMPT_COMMAND=__prompt_command

__prompt_command() {
	local EXIT="$?"
	if [ $EXIT != 0 ]; then
		PS1='[\[\033[31m\]\u\[\033[0m\]@\[\033[31m\]\h\[\033[0m\] \W]\[\033[33m\]$(__git_ps1 " (%s)")\[\033[0m\]\$ '
	else
		PS1='[\[\033[34m\]\u\[\033[0m\]@\[\033[34m\]\h\[\033[0m\] \W]\[\033[33m\]$(__git_ps1 " (%s)")\[\033[0m\]\$ '
	fi
}


if [ $(hostname) == "arch" ]; then
	export LIBVA_DRIVER_NAME=vdpau
	export VDPAU_DRIVER=radeonsi
elif [ $(hostname) == "htpc" ]; then
	export LIBVA_DRIVER_NAME=i965
	export VDPAU_DRIVER=va_gl
elif [ $(hostname) == "T430" ]; then
	source ~/dev/home2/hue/hue.sh
elif [ $(hostname) == "pi" ]; then
	source /home/robert/home2/hue/hue.sh
	source /home/robert/home2/sql.sh
fi
export EDITOR=vim
export TERM=xterm-256color
export BROWSER=chromium
export ANDROID_HOME=/opt/android-sdk

#if [[ -f ~/dev/hue/hue.sh ]]; then source ~/dev/hue/hue.sh; fi
